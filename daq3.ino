//DAQ Prototipo 2.0
//Jhubert T D
//2019
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#define DHTPIN 4
#define DHTTYPE    DHT11     // DHT 11

#include <LightDependentResistor.h>
#define OTHER_RESISTOR 10000 //ohms
#define USED_PIN A0
#define USED_PHOTOCELL LightDependentResistor::GL5528

// Create a GL5528 photocell instance (on A0 pin)
LightDependentResistor photocell(USED_PIN, OTHER_RESISTOR, USED_PHOTOCELL);

DHT_Unified dht(DHTPIN, DHTTYPE);


byte luz = 0; //declaracion de dato a leer
byte pinluz = 0;  //pin analogico a usar (tambien puede ser el 4)
byte pinhumedad = 4; //
byte pinvibracion = 3;
byte pintemperatura = 4;
int intrusion = false;
float vibracion = 0;
int UVOUT = A5; //Output from the sensor
int REF_3V3 = A4; //3.3V power on the Arduino board
int pinUV = A5;

void setup() {//funcion setup solo corre una vez
  pinMode(UVOUT, INPUT);
  pinMode(REF_3V3, INPUT);
  pinMode(13, OUTPUT);
  Serial.begin(9600); //puerto serial a 9600 velocidad
  dht.begin(); //inicia la libreria dht

}

void loop() {  //bucle loop se repite siempre


  int movimiento = analogRead(pinvibracion);

  if (movimiento < 500) { // sensibilidad de disparo de temblor
    digitalWrite(13, HIGH);
    intrusion = true;
    int contador = 10;
    vibracion = analogRead(pinvibracion); //guardamos el evento en una variable global
  }

  if (Serial.available() > 0) {
    char sensor = Serial.read();
    if (( sensor == 'L') || (sensor == 'l')) {//luz
      Serial.println(leerLuz(pinluz));

    }
    if (( sensor == 'H') || (sensor == 'h')) {//humedad
      Serial.println(leerHumedad(pinhumedad));

    }
    if (( sensor == 'V') || (sensor == 'v')) {//vibracion
      if (intrusion == true) {
        Serial.println(vibracion);
        digitalWrite(13, LOW);
        intrusion = false;
      }
      else {
        Serial.println(leerVibracion(pinvibracion));
      }

    }
    if (( sensor == 'T') || (sensor == 't')) {//temperatura
      Serial.println(leerTemperatura(pintemperatura));

    }

    if (( sensor == 'U') || (sensor == 'u')) {//UV
      Serial.println(leerUV(pinUV));

    }
  }
}

float leerLuz(int pin) {
  float intensity_in_lux = photocell.getCurrentLux();
  //float luzValor = analogRead(pin);
  //return luzValor;
  return (LightDependentResistor::luxToFootCandles(intensity_in_lux));

}

float leerHumedad(int pin) {
  sensors_event_t event;
  dht.humidity().getEvent(&event);
  return float(event.relative_humidity);
}

float leerVibracion(int pin) {
  float vibracionValor = analogRead(pin);
  return vibracionValor;
}

float leerTemperatura(int pin) {
  sensors_event_t event;
  dht.temperature().getEvent(&event);
  return float(event.temperature);
}

float leerUV(int pin) {
  int uvLevel = averageAnalogRead(UVOUT);
  int refLevel = averageAnalogRead(REF_3V3);
  //Use the 3.3V power pin as a reference to get a very accurate output value from sensor
  float outputVoltage = 3.3 / refLevel * uvLevel;
  float uvIntensity = mapfloat(outputVoltage, 0.99, 2.9, 0.0, 15.0);
  if (uvIntensity < 0) { //si la intensidad es negativa entonces es cero.
    uvIntensity = 0;
  }
  return uvIntensity;
}

//funciones para lectura de ultravioleta

int averageAnalogRead(int pinToRead) {
  byte numberOfReadings = 8;
  unsigned int runningValue = 0;
  for (int x = 0 ; x < numberOfReadings ; x++)
    runningValue += analogRead(pinToRead);
  runningValue /= numberOfReadings;
  return (runningValue);
}
//The Arduino Map function but for floats

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
